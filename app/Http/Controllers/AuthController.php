<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        // dd(Hash::make('dosen'));
        return view('auth.login');
    }

    public function auth(Request $req)
    {
        if (Auth::attempt(['username' => $req->username, 'password' => $req->password])) {
            return redirect()->route('dashboard');
        }
        return redirect('/')->with('message', 'Username atau password salah');
    }

    public function logout(Request $req)
    {
        Auth::logout();
        return redirect('/');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\KategoriModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class KategoriController extends Controller
{

    public function index(Request $req)
    {
        $kategori = KategoriModel::all();

        if ($req->ajax()) {
            return datatables()->of($kategori)
                ->addColumn('no', function () {
                })
                ->addColumn('action', function ($data) {

                    $button = '<a href="javascript:void(0)" id="' . $data->id . '" class="btn btn-warning edit"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp&nbsp';
                    // if ($data->status == null) {
                    //     $button .= '<button type="button" name="nonaktif" id="' . $data->id . '" class="nonaktif btn btn-danger"><i class="far fa-window-close"></i> Non-Aktifkan</button>';
                    // } else {
                    $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger"><i class="fas fa-trash"></i> </button>';
                    // }
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('kategori.index');
    }

    public function store(Request $req)
    {
        $kategori = DB::table('kategori')
            ->insert([
                'nama' => $req->nama,
            ]);


        return response()->json($kategori);
    }


    public function edit($id)
    {
        $kategori = DB::table('kategori')
            ->where('id', $id)
            ->first();
        return response()->json($kategori);
    }

    public function update(Request $req, $id)
    {
        $kategori = DB::table('kategori')
            ->where('id', $id)
            ->update([
                'nama' => $req->editNama,
            ]);
        return response()->json($kategori);
    }



    public function destroy($id)
    {
        $kategori = KategoriModel::findOrFail($id);
        $kategori->delete();
        return response()->json($kategori);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\KeperluanModel;
use Yajra\DataTables\Facades\DataTables;

class KeperluanController extends Controller
{

    public function json()
    {
        $keperluan = KeperluanModel::all();
        return Datatables::of($keperluan)->make(true);
    }
    public function index()
    {
        $keperluan = KeperluanModel::get();
        return view('keperluan.index', compact('keperluan'));
    }

    public function store(Request $req)
    {
        $keperluan = KeperluanModel::create([
            'nama' => $req->keperluan
        ]);
        return response()->json($keperluan);
    }

    public function delete($id)
    {
        $keperluan = KeperluanModel::findOrFail($id);
        $keperluan->delete();
        return response()->json($keperluan);
    }

    public function update(Request $req, $id)
    {

        $keperluan = KeperluanModel::findOrFail($id);

        $keperluan->update([
            'nama' => $req->editNama
        ]);
        return response()->json($keperluan);
    }

    public function edit($id)
    {
        $keperluan = KeperluanModel::findOrFail($id);
        return response()->json($keperluan);
    }
}

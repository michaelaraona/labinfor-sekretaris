<?php

namespace App\Http\Controllers;

use App\Models\GenerateModel;
use App\Models\KeperluanModel;
use App\Models\SuratModel;
use App\Models\TemplateModel;
use App\Models\TemporaryFile;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\Response;

class SuratController extends Controller
{
    public function __construct()
    {
        setlocale(LC_TIME, 'id_ID');
        \Carbon\Carbon::setLocale('id');
        \Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        // dd($nosurat = str_pad(1, 5, "0", STR_PAD_LEFT));
        // $today = Carbon::now()->isoFormat('D MMMM Y');
        array_map('unlink', glob(public_path('surat/tmp/') . '*.docx'));
        array_map('unlink', glob(public_path('surat/tmp/') . '*.pdf'));
        TemporaryFile::truncate();
        $surat = DB::table('surat')
            ->select('surat.*', DB::raw('IFNULL( kategori.nama,"Surat Masuk") as namaKategori'), 'template.nama as namaTemplate', DB::raw('IFNULL( keperluan.nama,"Surat Masuk") as keperluan'), 'users.name as namaUser')
            ->join('generate_surat', 'generate_surat.surat_id', 'surat.id')
            ->join('users', 'surat.user_id', 'users.id')
            ->leftJoin('template', 'generate_surat.template_id', 'template.id')
            ->leftJoin('kategori', 'template.kategori_id', 'kategori.id')
            ->leftJoin('keperluan', 'generate_surat.keperluan_id', 'keperluan.id')
            ->orderBy('surat.created_at',  'ASC')
            ->get();
        $keperluan = KeperluanModel::all();
        $template = DB::table('template')
            ->select('template.id as id', 'kategori.nama as kategori',  'template.nama as nama', 'template.file as file', 'template.status as status')
            ->join('kategori', 'kategori_id', 'kategori.id')
            ->where('template.status', NULL)
            ->get();

        if ($req->ajax()) {
            return datatables()->of($surat)
                ->addColumn('no', function () {
                })
                ->addColumn('status', function ($data) {
                    if (is_null($data->nama_file)) {
                        $status = '<span class="badge badge-danger">Belum Upload PDF File</span>';
                    } else {
                        $status = '<span class="badge badge-success">Surat sudah Ter-Arsip</span>';
                    }
                    return $status;
                })
                ->addColumn('action', function ($data) {
                    $button = '<center><button name="edit" id="' . $data->id . '" class="btn btn-warning edit"><i class="fas fa-pencil-alt"></i></button>';

                    $button .= '&nbsp&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger"><i class="fas fa-trash"></i></button>';

                    if (is_null($data->nama_file)) {
                        $button .= '&nbsp&nbsp';
                        $button .= '<button type="button" name="" id="' . $data->id . '" class="uploadPDF btn btn-success"><i class="mdi mdi-cloud-upload mr-1"></i>PDF</button>';
                    } else {
                        $button .= '&nbsp&nbsp';
                        $button .= '<button type="button" name="detail" id="' . $data->id . '" class="detail btn btn-info"><i class="fas fa-eye"></i></button>';
                        $button .= '&nbsp&nbsp';
                        $button .= '<button type="button" name="a" id="' . $data->id . '" class="deletePDF btn btn-outline-danger"><i class="fas fa-trash"></i> Delete PDF</button>';
                    }

                    $button .= '&nbsp&nbsp </center>';
                    return $button;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('suratkeluar.index', compact('template', 'keperluan'));
    }

    public function persuratanmasuk(Request $req)
    {
        array_map('unlink', glob(public_path('surat/tmp/') . '*.docx'));
        array_map('unlink', glob(public_path('surat/tmp/') . '*.pdf'));
        TemporaryFile::truncate();
        $surat = SuratModel::select('surat.*', 'users.name as namaUser')
            ->join('users', 'surat.user_id', 'users.id')
            ->where('jenis_surat', 2)
            ->get();
        $keperluan = KeperluanModel::get();
        $template = DB::table('template')
            ->select('template.id as id', 'kategori.nama as kategori',  'template.nama as nama', 'template.file as file', 'template.status as status')
            ->join('kategori', 'kategori_id', 'kategori.id')
            ->where('template.status', NULL)
            ->get();

        if ($req->ajax()) {
            return datatables()->of($surat)
                ->addColumn('no', function () {
                })
                ->addColumn('action', function ($data) {
                    $button = '<center><button name="edit" id="' . $data->id . '" class="btn btn-warning edit"><i class="fas fa-pencil-alt"></i></button>';
                    $button .= '&nbsp&nbsp';
                    // $button .= '<button type="button" id="' . $data->id . '" class="desc btn btn-info"><i class="fas fa-eye"></i></button>';
                    $button .= '<button type="button" name="detail" id="' . $data->id . '" class="detail btn btn-info"><i class="fas fa-eye"></i></button>';
                    $button .= '&nbsp&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger"><i class="fas fa-trash"></i></button>';
                    $button .= '&nbsp&nbsp </center>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('suratmasuk.index', compact('template', 'keperluan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
    }

    public function suratMasuk(Request $req)
    {
        $temporaryFile = TemporaryFile::where('filename', $req->docs)->first();
        $file =  now()->timestamp . '_suratmasuk_' . $temporaryFile->filename;

        if ($temporaryFile) {
            SuratModel::create([
                'nama' => $req->nama,
                'nama_file' => $file,
                'no_surat' => $req->nosurat,
                'tgl_masuk' => now(),
                'tgl_keluar' => Carbon::parse($req->tgl_dibuat)->toDateString('d-m-Y'),
                'keterangan' => $req->up_keterangan,
                'jenis_surat' => 2,
                'user_id' => Auth::user()->id,

            ]);

            // Pindah file
            File::move(public_path('surat/tmp/') . $temporaryFile->filename, public_path('surat/masuk/') . $file);
            $temporaryFile->delete();
        }
        return redirect('persuratan/masuk')->with('sukses', 'Berhasil Upload Surat Masuk');
    }

    public function uploadPDF(Request $req)
    {
        array_map('unlink', glob(public_path('surat/generate/') . '*.docx'));
        $temporaryFile = TemporaryFile::where('filename', $req->docs)->first();
        $file =  now()->timestamp . '_' . $temporaryFile->filename;
        $surat = SuratModel::find($req->id_surat);

        if ($temporaryFile) {
            $surat->nama_file = $file;
            $surat->tgl_masuk = now();
            $surat->save();

            // Pindah file
            File::move(public_path('surat/tmp/') . $temporaryFile->filename, public_path('surat/pdf_upload/') . $file);
            $temporaryFile->delete();
        }
        return response()->json($surat);
    }

    public function hapusPDF($id)
    {
        $surat = SuratModel::find($id);

        unlink(public_path('surat/pdf_upload/') . $surat->nama_file);
        $surat->nama_file = NULL;
        $surat->save();
        return response()->json($surat);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $surat = SuratModel::findOrFail($id);
        return response()->json($surat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $surat = SuratModel::find($id);
        if ($surat->jenis_id == 1) {
            $surat->update([
                'no_surat' => $req->edit_no_surat,
                'nama' => $req->edit_nama,
                'keterangan' => $req->edit_keterangan,
            ]);
        } else {
            $surat->update([
                'no_surat' => $req->edit_no_surat,
                'nama' => $req->edit_nama,
                'keterangan' => $req->edit_keterangan,
                'tgl_keluar' => Carbon::parse($req->edit_tgl_dibuat)->toDateString(),
            ]);
        }

        $surat->save();

        return response()->json($surat);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $surat = SuratModel::where('id', $id)->first();
        if ($surat->jenis_surat == 1 && $surat->nama_file != NULL) {
            if (file_exists(public_path('surat/pdf_upload/') . $surat->nama_file)) {
                // unlink(public_path('surat\\') . $surat->nama_file);
                unlink(public_path('surat/pdf_upload/') . $surat->nama_file);
            }
        } else if ($surat->jenis_surat == 2 && $surat->nama_file != NULL) {
            if (file_exists(public_path('surat/masuk/') . $surat->nama_file)) {
                // unlink(public_path('surat\\') . $surat->nama_file);
                unlink(public_path('surat/masuk/') . $surat->nama_file);
            }
        }
        $surat->delete();

        return response()->json($surat);
    }

    public function process(Request $req)
    {
        if ($req->hasFile('docs')) {
            $file = $req->file('docs');
            $filename = $file->getClientOriginalName();
            $file->move(public_path('surat/tmp'), $filename);

            TemporaryFile::create([
                'folder' => '',
                'filename' => $filename
            ]);
            return $filename;
        }

        return '';
    }

    public function revert()
    {
        $temporaryFile = TemporaryFile::where('filename', request()->getContent())->first();
        if (file_exists(public_path('surat/tmp/') . request()->getContent())) {
            unlink(public_path('surat/tmp/') . request()->getContent());
            $temporaryFile->delete();
        }
    }

    public function download($file)
    {
        $surat = SuratModel::where('nama_file', $file)->firstOrFail();
        if ($surat->jenis_surat == 1) {
            return response()->file(public_path('surat/pdf_upload/') . $surat->nama_file, [
                'Content-Type' => 'application/pdf'
            ]);
        } else {
            return response()->file(public_path('surat/masuk/') . $surat->nama_file, [
                'Content-Type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            ]);
        }
        // dd($surat);
    }

    public function generate(Request $req)
    {
        $template = DB::table('template')
            ->select('template.id as id', 'kategori.nama as kategori', 'template.nama as nama', 'template.file as file',  'template.status as status')
            ->join('kategori', 'kategori_id', 'kategori.id')
            ->where('template.status', NULL)
            ->where('template.id', $req->g_template)
            ->first();

        $docs = public_path('surat/template/') . $template->file;
        if (file_exists($docs)) {
            $tanggal = Carbon::parse($req->g_tgl)->isoFormat("D MMMM YYYY");

            $templateProcessor = new TemplateProcessor($docs);
            $templateProcessor->setValue('no_surat', $req->g_nosurat);
            $templateProcessor->setValue('tanggalSurat', $tanggal);

            $file_name = now()->timestamp . '_' . $req->g_nama . '.docx';
            $templateProcessor->saveAs($file_name);

            $surat_create = SuratModel::create([
                'nama' => $req->g_nama,
                'no_surat' => $req->g_nosurat,
                'tgl_keluar' => now(),
                'jenis_surat' => 1,
                'user_id' => Auth::user()->id,
                'keterangan' => $req->keterangan,

            ]);

            // ambil id dari atas
            GenerateModel::create([
                'template_id' => $template->id,
                'surat_id' => $surat_create->id,
                'keperluan_id' => $req->g_keperluan,

            ]);
            // Pindah file
            File::move(public_path() . '/' . $file_name, public_path('surat/generate/') . $file_name);

            return response()->json([
                "file_name" => url('') . '/surat/generate/' . $file_name,
            ]);
        }
    }

    public function generate_nosurat($keperluan_id)
    {
        $array_bln = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
        $bln = $array_bln[date('n')];
        $keperluan = KeperluanModel::where('id', $keperluan_id)->first();
        $surat = GenerateModel::orderByDesc('id')
            ->limit(1)
            ->first();
        if (is_null($surat)) {
            $nosurat = str_pad(1, 4, "0", STR_PAD_LEFT) . '/' . $keperluan->nama . '/' . $bln . '/' . date('Y');
        } else {
            $nosurat = str_pad($surat->id++, 4, "0", STR_PAD_LEFT) . '/' . $keperluan->nama . '/' . $bln . '/' . date('Y');
        }
        // $nosurat = is_null($surat) ? '1' . '/' . $keperluan->nama . '/' . $bln . '/' . date('Y') : $surat->id + 1;
        return response()->json($nosurat);
    }
}

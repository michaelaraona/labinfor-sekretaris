<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;
use Alert;
use App\Models\KategoriModel;
use App\Models\TemplateModel;
use App\Models\TemporaryFile;
use Exception;


class TemplateController extends Controller
{
    //
    public function index(Request $req)
    {
        array_map('unlink', glob(public_path('surat/tmp/') . '*.docx'));
        array_map('unlink', glob(public_path('surat/tmp/') . '*.pdf'));
        TemporaryFile::truncate();
        $template = DB::table('template')
            ->select('template.id as id', 'kategori.nama as kategori',  'template.nama as nama', 'template.file as file', 'template.status as status')
            ->join('kategori', 'kategori_id', 'kategori.id')
            ->where('template.status', NULL)
            ->get();
        $kategori = KategoriModel::all();

        if ($req->ajax()) {
            return datatables()->of($template)
                ->addColumn('no', function () {
                })
                ->addColumn('action', function ($data) {

                    // if ($data->kategoriStatus == null) {
                    $button = '<center><a href="javascript:void(0)" id="' . $data->id . '" class="btn btn-warning edit"><i class="fas fa-pencil-alt"></i></a>';
                    $button .= '&nbsp&nbsp';
                    $button .= '<a type="button" href="' . route('preview', $data->file) . '" target="_BLANK" class="desc btn btn-info"><i class="fas fa-download"></i></a>';
                    // $button .= '<button type="button" name="desc" id="' . $data->id . '" class="desc btn btn-info"><i class="fas fa-eye"></i></button>';
                    $button .= '&nbsp&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger"><i class="fas fa-trash"></i></button>';
                    $button .= '&nbsp&nbsp';
                    // } else {
                    // $button .= '<center><span class="badge badge-danger">Kategori Non-Aktif</span>';
                    // }
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('template.index', compact('kategori'));
    }
    public function create()
    {
        $kategori = KategoriModel::all();
        return view('template.create', compact('kategori'));
    }

    public function store(Request $req)
    {
        $temporaryFile = TemporaryFile::where('filename', $req->docs)->first();
        $file =  now()->timestamp . '_template_' . $temporaryFile->filename;
        if ($temporaryFile) {
            TemplateModel::create([
                'kategori_id' => $req->kategori,
                'nama' => $req->nama,
                'file' => $file
            ]);

            // rmdir(public_path('surat/tmp/' . $req->docs));
            // Pindah file
            File::move(public_path('surat/tmp/') . $temporaryFile->filename, public_path('surat/template/') . $file);
            $temporaryFile->delete();
        }
        return redirect('template')->with('sukses', 'Berhasil Upload Template');
    }

    public function edit($id)
    {
        $template = DB::table('template')
            ->select('template.id as id', 'template.kategori_id as idKategori', 'kategori.nama as kategori', 'template.nama as nama', 'template.file as file')
            ->join('kategori', 'kategori_id', 'kategori.id')
            ->where('template.id', $id)
            ->first();
        $kategori = KategoriModel::all();

        return view('template.edit', compact('template', 'kategori'));
    }

    public function update(Request $req, $id)
    {
        DB::table('template')
            ->where('id', $id)
            ->update([
                'kategori_id' => $req->kategori,
                'nama' => $req->nama
            ]);
        return redirect('template')->with('sukses', 'Berhasil Update Template');
    }

    public function delete($id)
    {
        $template = DB::table('template')
            ->where('id', $id)
            ->update([
                'status' => 1,
            ]);
        return response()->json($template);
    }

    public function trash(Request $req)
    {
        $trash = DB::table('template')
            ->select('template.id as id', 'kategori.nama as kategori', 'template.nama as nama', 'template.file as file', 'template.status as status')
            ->join('kategori', 'kategori_id', 'kategori.id')
            ->where('template.status', 1)
            ->get();
        if ($req->ajax()) {
            return datatables()->of($trash)
                ->addColumn('no', function () {
                })
                ->addColumn('action', function ($data) {
                    $button = '<center><button type="button" id="btnPulihkan" idPulihkan="' . $data->id . '" class="btn btn-success">Pulihkan</button></center>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function pulihkan($id)
    {
        $template = DB::table('template')
            ->where('id', $id)
            ->update([
                'status' => null,
            ]);
        return response()->json($template);
    }

    public function show($id)
    {
        $template = DB::table('template')
            ->where('id', $id)
            ->first();
        return response()->json($template);
    }

    public function process(Request $req)
    {
        if ($req->hasFile('docs')) {
            $file = $req->file('docs');
            $filename = $file->getClientOriginalName();
            $file->move(public_path('surat/tmp'), $filename);

            TemporaryFile::create([
                'folder' => '',
                'filename' => $filename
            ]);
            return $filename;
        }

        return '';
    }

    public function revert()
    {
        $temporaryFile = TemporaryFile::where('filename', request()->getContent())->first();
        if (file_exists(public_path('surat/tmp/') . request()->getContent())) {
            unlink(public_path('surat/tmp/') . request()->getContent());
            $temporaryFile->delete();
        }
    }

    public function preview($file)
    {
        $template = TemplateModel::where('file', $file)->firstOrFail();
        return response()->file(public_path('surat/template/') . $template->file, [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        ]);
        // dd($template->file);
    }

    public function contoh()
    {
        return response()->file(public_path('surat/contoh/') . "contoh_template.docx", [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        ]);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class DosenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->role == 1) {
            return $next($request);
        }
        // return $next($request);
        return redirect('dashboard')->with('error', 'Anda Tidak Dapat Akses Halaman Ini :)');
    }
}

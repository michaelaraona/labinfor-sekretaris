<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GenerateModel extends Model
{
    use HasFactory;
    protected $table = 'generate_surat';
    public $primaryKey = 'id';
    protected $fillable = [
        'template_id',
        'surat_id',
        'keperluan_id',
    ];
}

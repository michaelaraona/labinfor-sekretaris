<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KeperluanModel extends Model
{
    protected $table = 'keperluan';
    public $primaryKey = 'id';
    protected $fillable = [
        'nama',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratModel extends Model
{
    use HasFactory;

    protected $table = "surat";
    protected $fillable = ['nama', 'nama_file', 'no_surat', 'bukti_gambar', 'tgl_masuk', 'tgl_keluar', 'keterangan', 'jenis_surat', 'user_id'];
}

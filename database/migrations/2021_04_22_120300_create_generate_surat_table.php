<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenerateSuratTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generate_surat', function (Blueprint $table) {
            $table->id();
            $table->foreignId('template_id')
                ->nullable()
                ->constrained('template')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreignId('surat_id')
                ->nullable()
                ->constrained('surat')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('keperluan_id')
                ->constrained('keperluan')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generate_surat');
    }
}

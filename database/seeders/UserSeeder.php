<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
            ->insert(
                [
                    [
                        'name' => 'Sekretaris',
                        'username' => 'sekretaris',
                        'password' => Hash::make('koorrplgantengbanget'),
                        'role' => 0,
                    ],
                    [
                        'name' => 'Dosen',
                        'username' => 'dosen',
                        'password' => Hash::make('koorrplgantengbanget'),
                        'role' => 1,
                    ],
                ]
            );
    }
}

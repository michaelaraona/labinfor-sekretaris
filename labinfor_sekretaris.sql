-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 30, 2021 at 12:08 AM
-- Server version: 5.7.35
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `labinfor_sekretaris`
--

-- --------------------------------------------------------

--
-- Table structure for table `generate_surat`
--

CREATE TABLE `generate_surat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `template_id` bigint(20) UNSIGNED DEFAULT NULL,
  `surat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `keperluan_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `generate_surat`
--

INSERT INTO `generate_surat` (`id`, `template_id`, `surat_id`, `keperluan_id`, `created_at`, `updated_at`) VALUES
(2, 4, 3, 2, '2021-06-28 10:44:16', '2021-06-28 10:44:16'),
(9, 5, 14, 2, '2021-09-25 06:08:57', '2021-09-25 06:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(3, 'Surat Masuk', NULL, NULL),
(4, 'Surat Keluar', NULL, NULL),
(7, 'KP2021', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `keperluan`
--

CREATE TABLE `keperluan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `keperluan`
--

INSERT INTO `keperluan` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'LRPL', '2021-06-25 19:08:53', '2021-06-25 19:08:53'),
(2, 'LRPL/ITATS', '2021-06-28 07:40:52', '2021-06-28 07:40:52'),
(5, 'KP/2021', '2021-07-22 02:22:17', '2021-07-22 02:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_02_10_051259_create_users_table', 1),
(2, '2021_02_10_051930_create_keperluan_table', 1),
(3, '2021_02_10_052614_create_kategori_table', 1),
(4, '2021_02_21_034840_create_template_table', 1),
(5, '2021_04_11_032655_create_temporary_files_table', 1),
(6, '2021_04_22_103210_create_surat_table', 1),
(7, '2021_04_22_120300_create_generate_surat_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `surat`
--

CREATE TABLE `surat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_file` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_surat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_surat` int(11) DEFAULT NULL,
  `tgl_masuk` datetime DEFAULT NULL,
  `tgl_keluar` datetime DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `surat`
--

INSERT INTO `surat` (`id`, `nama`, `nama_file`, `no_surat`, `jenis_surat`, `tgl_masuk`, `tgl_keluar`, `keterangan`, `user_id`, `created_at`, `updated_at`) VALUES
(3, 'Pendaftaran Asisten Lab Rekayasa Perangkat Lunak', '1624877205_PENDAFTARAN_ASLAB(001.LRPL.ITATAS.II.2021).pdf', '0001/LRPL/ITATS/VI/2021', 1, '2021-06-28 17:46:45', '2021-09-22 00:00:00', 'Surat keluar tentang pendaftaran asisten lab rekayasa perangkat lunak 2021/2022', 1, '2021-06-28 10:44:16', '2021-09-22 10:30:16'),
(4, 'Surat Tugas Pelaksanaan Praktikum Basis Data 2021', '1624877620_suratmasuk_SuratTugas.docx', '212.A/JTC/ITATS/III/2021', 2, '2021-06-28 17:53:40', '2021-06-27 00:00:00', 'Surat tugas mengenai pelaksanaan praktikum basis data 2021', 1, '2021-06-28 10:53:40', '2021-07-21 14:28:04'),
(5, 'Surat Keputusan Praktikum', '1624878395_suratmasuk_No.00003 SURAT KEPUTUSAN.docx', '00003/ LRPL/ITATS/IV/2021', 2, '2021-06-28 18:06:35', '2021-06-28 00:00:00', 'Surat keputusan Lab tentang duplikasi laporan langkah percobaan', 2, '2021-06-28 11:06:35', '2021-06-28 11:06:35'),
(14, 'PENGUMUMAN PRAKTIKUM PBO 2021', NULL, '0000002/LRPL/ITATS/IX/2021', 1, NULL, '2021-09-27 00:00:00', 'SURAT PENGUMUMAN PRAKTIKUM PBO 2021 PERIODE VII', 1, '2021-09-25 06:08:57', '2021-09-27 07:35:54');

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE `template` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kategori_id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`id`, `kategori_id`, `nama`, `file`, `status`, `created_at`, `updated_at`) VALUES
(4, 4, 'PENDAFTARAN ASISTEN LAB REKAYASA PERANGKAT LUNAK (RPL)', '1624866600_template_PENDAFTARAN_ASLAB.docx', NULL, '2021-06-28 07:50:00', '2021-06-28 07:50:00'),
(5, 4, 'PENGUMUMAN PRAKTIKUM', '1624878899_template_Pengumuman Praktikumm Basis Data.docx', NULL, '2021-06-28 11:14:59', '2021-06-28 11:14:59'),
(6, 4, 'tesis', '1626876821_template_PENDAFTARAN_ASLAB.docx', NULL, '2021-07-21 14:13:41', '2021-07-21 14:13:41'),
(7, 4, 'Peminjaman Ruangan Lab 2021', '1626920680_template_Template Peminjaman Ruangan Lab.docx', NULL, '2021-07-22 02:24:40', '2021-07-22 02:24:40');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_files`
--

CREATE TABLE `temporary_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `folder` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Sekretaris', 'sekretaris', '$2y$10$NHb5qKBBRMHagUMj5wcJqu6B5bVNXaOBnIyuka/LrKPqOYOo35Dz6', 0, NULL, NULL),
(2, 'Dosen', 'dosen', '$2y$10$w/HkuowYTrfwiIXVbKkZ6OxAcrcSA8gXaGldY7sF/pbTXqGM44ZP2', 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `generate_surat`
--
ALTER TABLE `generate_surat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `generate_surat_template_id_foreign` (`template_id`),
  ADD KEY `generate_surat_surat_id_foreign` (`surat_id`),
  ADD KEY `generate_surat_keperluan_id_foreign` (`keperluan_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keperluan`
--
ALTER TABLE `keperluan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `surat_user_id_foreign` (`user_id`);

--
-- Indexes for table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `template_kategori_id_foreign` (`kategori_id`);

--
-- Indexes for table `temporary_files`
--
ALTER TABLE `temporary_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `generate_surat`
--
ALTER TABLE `generate_surat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `keperluan`
--
ALTER TABLE `keperluan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `template`
--
ALTER TABLE `template`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `temporary_files`
--
ALTER TABLE `temporary_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `generate_surat`
--
ALTER TABLE `generate_surat`
  ADD CONSTRAINT `generate_surat_keperluan_id_foreign` FOREIGN KEY (`keperluan_id`) REFERENCES `keperluan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `generate_surat_surat_id_foreign` FOREIGN KEY (`surat_id`) REFERENCES `surat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `generate_surat_template_id_foreign` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `surat`
--
ALTER TABLE `surat`
  ADD CONSTRAINT `surat_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `template`
--
ALTER TABLE `template`
  ADD CONSTRAINT `template_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

@extends('layout/main')
@push('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('title', 'Kategori')
@section('navbarcontent')
    <h4 class="page-title">Kategori Surat</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('kategori') }}">Kategori</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>
    </div>
@endsection

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title mb-5">
                    <div class="col-md-4  float-left">
                        <h3>Data Kategori</h3>
                    </div>
                    <div class="col-md-6  float-right">
                        <button href="javascript:void(0)" class="btn btn-primary float-right" id="btnAdd"><i
                                class="fa fa-plus ml-1 mr-1"></i>Kategori</button>
                        {{-- Modal --}}
                        <div class="modal fade" id="modal-add" role="dialog" aria-labelledby="myLargeModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="form-tambah" name="form-tambah">
                                            <div class="form-group">
                                                <label for="nama" class="col-form-label">Nama Kategori</label>
                                                <input type="text" class="form-control" name="nama" id="nama"
                                                    autocomplete="off" required>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-success" id="btnSave">Tambah</button>
                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                        {{-- End Modal --}}
                    </div>
                </div>

                <div class="table-responsive">
                    <table id="" class="table table-striped table-bordered tableKategori">
                        <thead>
                            <tr>
                                <th width="5%">No.</th>
                                <th>Kategori Surat</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                    </table>
                </div>

                {{-- MODAL --}}
                <div class="modal fade" tabindex="-1" role="dialog" id="delete-modal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Konfirmasi Delete Kategori</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-footer bg-whitesmoke br">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-danger" name="btnDelete" id="btnDelete">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- End Modal --}}

                {{-- Modal --}}
                <div class="modal fade" id="modal-edit" role="dialog" aria-labelledby="myLargeModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Kategori</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="form-edit" name="form-edit">
                                    @method('put')
                                    <input type="hidden" name="id" id="id">
                                    <div class="form-group">
                                        <label for="nama" class="col-form-label">Nama Kategori</label>
                                        <input type="text" class="form-control" name="editNama" id="editNama"
                                            autocomplete="off" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success" id="btnSaveEdit">Simpan Edit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- End Modal --}}
            </div>
        </div>
    @endsection

    @push('after-script')
        <script>
            // $('.btn-edit').on('click', function(){
            //     let id = $(this).data('id');
            //     $.ajax({ 
            //         url:`/kategori/edit/${id}`,
            //         method:"GET",
            //         success: function(data){
            //             $('#modal-edit').find('.modal-body').html(data);
            //             $('#modal-edit').modal('show');
            //         },
            //         error: function(error){
            //             console.log(error);
            //         }
            //     });
            // });

            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var t = $('.tableKategori').DataTable({
                    lengthMenu: [
                        [5, 10, 50, -1],
                        [5, 10, 50, "All"]
                    ],
                    bAutoWidth: false,
                    processing: true,
                    serverSide: true,
                    language: {
                        processing: '<div class="fas fa-circle-notch fa-3x fa-spin style="display: inline-block;"></div>'
                    },
                    ajax: {
                        url: "{{ url('/kategori') }}",
                        type: "GET",
                    },
                    columns: [{
                            data: 'no',
                            name: 'no'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        },
                    ],

                });
                t.on('draw.dt', function() {
                    var PageInfo = $('.tableKategori').DataTable().page.info();
                    t.column(0, {
                        page: 'current'
                    }).nodes().each(function(cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                    });
                });


            });
            // Add
            $('#btnAdd').click(function() {
                $('#btnSave').html('Tambah');
                var x = document.getElementById("btnSave");
                x.disabled = false;
                $('#modal-add').modal('show'); //modal tampil
            });


            $("#form-tambah").validate({
                submitHandler: function(form) {
                    $('#btnSave').html('Sending..');
                    var x = document.getElementById("btnSave");
                    x.disabled = true;
                    $.ajax({
                        data: $('#form-tambah')
                            .serialize(), //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request

                        url: "{{ url('/kategori/store') }}", //url simpan data
                        type: "POST", //karena simpan kita pakai method POST
                        dataType: 'json', //data tipe kita kirim berupa JSON
                        success: function(data) { //jika berhasil 
                            $('#form-tambah').trigger("reset"); //form reset
                            $('#modal-add').modal('hide'); //modal hide
                            var oTable = $('.tableKategori').dataTable(); //inialisasi datatable
                            oTable.fnDraw(false); //reset datatable
                            iziToast.success({ //tampilkan iziToast dengan notif data berhasil disimpan pada posisi kanan bawah
                                title: 'Data Kategori Berhasil Disimpan',
                                message: '{{ Session('
                            success ') }}',
                                position: 'bottomRight'
                            });
                        },
                        error: function(data) { //jika error tampilkan error pada console
                            console.log('Error:', data);
                        }
                    });
                }
            });



            // Edit========================================================================
            $('body').on('click', '.edit', function() {
                data_id = $(this).attr('id');
                $('#btnSaveEdit').html('Simpan Edit');
                var x = document.getElementById("btnSaveEdit");
                x.disabled = false;
                $.get(`{{ route('kategori.edit', '') }}` + '/' + data_id, function(data) {
                    $('#modal-edit').modal('show');

                    $('#id').val(data.id);
                    $('#editNama').val(data.nama);
                })
            });

            $("#form-edit").validate({
                submitHandler: function(form) {
                    $('#btnSaveEdit').html('Sending..');
                    var x = document.getElementById("btnSaveEdit");
                    x.disabled = true;
                    $.ajax({
                        data: $('#form-edit')
                            .serialize(), //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request

                        url: "{{ url('') }}/kategori/update/" + data_id, //url simpan data
                        type: "POST", //karena simpan kita pakai method POST
                        dataType: 'json', //data tipe kita kirim berupa JSON
                        success: function(data) { //jika berhasil 
                            $('#form-edit').trigger("reset"); //form reset
                            $('#modal-edit').modal('hide'); //modal hide
                            var oTable = $('.tableKategori').dataTable(); //inialisasi datatable
                            oTable.fnDraw(false); //reset datatable
                            iziToast.success({ //tampilkan iziToast dengan notif data berhasil disimpan pada posisi kanan bawah
                                title: 'Data Kategori Berhasil Diedit',
                                message: '{{ Session('
                            success ') }}',
                                position: 'bottomRight'
                            });
                        },
                        error: function(data) { //jika error tampilkan error pada console
                            console.log('Error:', data);
                        }
                    });
                }
            })




            $(document).on('click', '.delete', function() {
                dataId = $(this).attr('id');
                $('#btnDelete').text('Delete');

                var x = document.getElementById("btnDelete");
                x.disabled = false;
                $('#delete-modal').modal('show');
            });

            // Aktif
            $('#btnDelete').click(function() {
                $.ajax({

                    url: "{{ url('') }}/kategori/destroy/" + dataId, //eksekusi ajax ke url ini
                    type: 'GET',
                    beforeSend: function() {
                        $('#btnDelete').text('Tunggu..'); //set text untuk tombol hapus
                        var x = document.getElementById("btnDelete");
                        x.disabled = true;
                    },
                    success: function(data) { //jika sukses
                        setTimeout(function() {
                            $('#delete-modal').modal('hide'); //sembunyikan konfirmasi modal
                            var oTable = $('.tableKategori').dataTable();
                            oTable.fnDraw(false); //reset datatable
                        });
                        iziToast.success({ //tampilkan izitoast warning
                            title: 'Data Kategori Berhasil Dihapus',
                            message: '{{ Session('
                        delete ') }}',
                            position: 'bottomRight'
                        });
                    }
                })
            });
        </script>
    @endpush

@extends('layout/main')
@section('title', 'Keperluan')
    @push('meta')
        <meta name="csrf-token" content="{{ csrf_token() }}">
    @endpush
@section('navbarcontent')
    <h4 class="page-title">Keperluan Surat</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('keperluan') }}">Keperluan</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>
    </div>
@endsection

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title mb-5">
                    <div class="col-md-4  float-left">
                        <h3>Data Keperluan</h3>
                    </div>
                    <div class="col-md-6  float-right">
                        <button class="btn btn-primary float-right" id="btnAdd"><i
                                class="fa fa-plus ml-1 mr-2"></i>Keperluan</button>
                        {{-- Modal --}}
                        <div class="modal fade" id="modal-add" role="dialog" aria-labelledby="myLargeModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Tambah Keperluan Surat</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="form-tambah" name="form-tambah">
                                            <div class="form-group">
                                                <label for="keperluan" class="col-form-label">Keperluan Surat</label>
                                                <input type="text" class="form-control" name="keperluan" id="keperluan"
                                                    autocomplete="off" required>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-success" id="btnSave">Tambah</button>
                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                        {{-- End Modal --}}
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped tableKeperluan">
                        <thead>
                            <tr>
                                <th width="70%">Nama</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    {{-- <table class="table table-striped tableKeperluan">
                        <thead>
                            <tr>
                                <th width="10%">No.</th>
                                <th width="70%">Nama</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($keperluan as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->nama }}</td>
                                    <td>
                                        <button type="button" name="delete" id="{{ $row->id }}"
                                            class="delete btn btn-danger"><i class="fas fa-trash"></i></button>
                                        <button type="button" name="edit" id="{{ $row->id }}"
                                            class="edit btn btn-warning"><i class="fas fa-pencil-alt"></i></button>

                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table> --}}
                </div>

                {{-- MODAL --}}
                <div class="modal fade" tabindex="-1" role="dialog" id="delete-modal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Konfirmasi Delete Keperluan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-footer bg-whitesmoke br">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-danger" name="btnDelete" id="btnDelete">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- End Modal --}}

                {{-- Modal --}}
                <div class="modal fade" id="modal-edit" role="dialog" aria-labelledby="myLargeModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Keperluan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="form-edit" name="form-edit">
                                    @method('put')
                                    <input type="hidden" name="id" id="id">
                                    <div class="form-group">
                                        <label for="nama" class="col-form-label">Nama Keperluan</label>
                                        <input type="text" class="form-control" name="editNama" id="editNama"
                                            autocomplete="off" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success" id="btnSaveEdit">Simpan Edit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- End Modal --}}
            </div>
        </div>
    @endsection

    @push('after-script')

        <script>
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var table = $('.tableKeperluan').DataTable({
                    lengthMenu: [
                        [5, 10, 50, -1],
                        [5, 10, 50, "All"]
                    ],
                    bAutoWidth: false,
                    processing: true,
                    serverSide: true,
                    ajax: '{{ url('') }}/keperluan/json',
                    deferRender: true,
                    columns: [


                        {
                            data: 'nama',
                            name: 'nama',
                            searchable: true
                        },
                        {
                            render: function(data, type, row) {

                                // console.log(data);
                                return '<div class="row">' +
                                    '<a href="javascript:void(0)" id="' + row['id'] +
                                    '" class="btn btn-warning edit"><i class="fas fa-pencil-alt"></i></a>' +
                                    '  &nbsp;' +
                                    ' <button type="button" id="' + row['id'] +
                                    '" class="btn btn-danger delete "><i class="fas fa-trash"></i> </button>' +
                                    '&nbsp;'

                            },
                        },
                    ],
                });

            });
            // Add
            $('#btnAdd').click(function() {
                $('#btnSave').html('Tambah');
                $('#modal-add').modal('show'); //modal tampil
                var x = document.getElementById("btnSave");
                x.disabled = false;
            });

            $("#form-tambah").validate({
                submitHandler: function(form) {
                    $('#btnSave').html('Sending..');
                    var x = document.getElementById("btnSave");
                    x.disabled = true;
                    $.ajax({
                        data: $('#form-tambah')
                            .serialize(), //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request
                        url: "{{ url('/keperluan/store') }}", //url simpan data
                        type: "POST", //karena simpan kita pakai method POST
                        dataType: 'json', //data tipe kita kirim berupa JSON
                        success: function(data) { //jika berhasil 
                            $('#form-tambah').trigger("reset"); //form reset
                            $('#modal-add').modal('hide'); //modal hide
                            var oTable = $('.tableKeperluan').dataTable(); //inialisasi datatable
                            oTable.fnDraw(false); //reset datatable
                            iziToast.success({ //tampilkan iziToast dengan notif data berhasil disimpan pada posisi kanan bawah
                                title: 'Data Keperluan Berhasil Disimpan',
                                message: '{{ Session('success') }}',
                                position: 'bottomRight'
                            });
                        },
                        error: function(data) { //jika error tampilkan error pada console
                            console.log('Error:', data);
                        }
                    });
                }
            });

            $(document).on('click', '.delete', function() {
                dataId = $(this).attr('id');
                $('#btnDelete').text('Delete');
                $('#delete-modal').modal('show');
                var x = document.getElementById("btnDelete");
                x.disabled = false;
            });

            // Delete
            $('#btnDelete').click(function() {
                $.ajax({

                    url: "{{ url('') }}/keperluan/delete/" + dataId, //eksekusi ajax ke url ini
                    type: 'GET',
                    beforeSend: function() {
                        $('#btnDelete').text('Tunggu..'); //set text untuk tombol hapus
                        var x = document.getElementById("btnDelete");
                        x.disabled = true;
                    },
                    success: function(data) { //jika sukses
                        $('#delete-modal').modal('hide'); //sembunyikan konfirmasi modal
                        var oTable = $('.tableKeperluan').dataTable(); //inialisasi datatable
                        oTable.fnDraw(false); //reset datatable
                        iziToast.success({ //tampilkan izitoast warning
                            title: 'Data Keperluan Berhasil Dihapus',
                            message: '{{ Session('
                        delete ') }}',
                            position: 'bottomRight'
                        });
                    }
                })
            });

            // Edit========================================================================
            $('body').on('click', '.edit', function() {
                data_id = $(this).attr('id');
                $('#btnSaveEdit').html('Simpan Edit');
                var x = document.getElementById("btnSaveEdit");
                x.disabled = false;
                $.get('{{ url('') }}/keperluan/edit/' + data_id, function(data) {
                    $('#modal-edit').modal('show');

                    $('#id').val(data.id);
                    $('#editNama').val(data.nama);
                });
            });

            $("#form-edit").validate({
                submitHandler: function(form) {
                    $('#btnSaveEdit').html('Sending..');
                    var x = document.getElementById("btnSaveEdit");
                    x.disabled = true;
                    $.ajax({
                        data: $('#form-edit')
                            .serialize(), //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request

                        url: "{{ url('') }}/keperluan/update/" + data_id, //url simpan data
                        type: "POST", //karena simpan kita pakai method POST
                        dataType: 'json', //data tipe kita kirim berupa JSON
                        success: function(data) { //jika berhasil 
                            $('#form-edit').trigger("reset"); //form reset
                            $('#modal-edit').modal('hide'); //modal hide
                            var oTable = $('.tableKeperluan').dataTable(); //inialisasi datatable
                            oTable.fnDraw(false); //reset datatable

                            iziToast.success({ //tampilkan iziToast dengan notif data berhasil disimpan pada posisi kanan bawah
                                title: 'Data Keperluan Berhasil Diedit',
                                message: '{{ Session('
                            success ') }}',
                                position: 'bottomRight'
                            });
                        },
                        error: function(data) { //jika error tampilkan error pada console
                            console.log('Error:', data);
                        }
                    });
                }
            });
        </script>

    @endpush

 <!-- Sidebar scroll-->
 <div class="scroll-sidebar">
     <!-- Sidebar navigation-->
     <nav class="sidebar-nav">
         <ul id="sidebarnav" class="p-t-30">
             <li class="sidebar-item">
                 <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('dashboard') }}"
                     aria-expanded="false">
                     <i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span>
                 </a>
             </li>
             @if (Auth::user()->role == 0)
                 <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark"
                         href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-cards"></i><span
                             class="hide-menu">Data Master </span></a>
                     <ul aria-expanded="false" class="collapse first-level">
                         <li class="sidebar-item">
                             <a href="{{ url('keperluan') }}" class="sidebar-link " aria-expanded="false">
                                 <i class="fas fa-circle"></i>
                                 <span class="hide-menu"> Keperluan </span>
                             </a>
                         </li>
                         <li class="sidebar-item">
                             <a href="{{ url('kategori') }}" class="sidebar-link " aria-expanded="false">
                                 <i class="fas fa-circle"></i>
                                 <span class="hide-menu"> Kategori </span>
                             </a>
                         </li>
                         <li class="sidebar-item">
                             <a href="{{ url('template') }}" class="sidebar-link " aria-expanded="false">
                                 <i class="fas fa-circle"></i>
                                 <span class="hide-menu"> Template Surat </span>
                             </a>
                         </li>
                     </ul>
                 </li>
             @endif
             <li class="sidebar-item">
                 <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
                     aria-expanded="false">
                     <i class="mdi mdi-cards"></i>
                     <span class="hide-menu">Persuratan</span>
                 </a>
                 <ul aria-expanded="false" class="collapse first-level">
                     <li class="sidebar-item">
                         <a href="{{ url('persuratan') }}" class="sidebar-link " aria-expanded="false">
                             <i class="fas fa-circle"></i>
                             <span class="hide-menu"> Surat Keluar </span>
                         </a>
                     </li>
                     <li class="sidebar-item">
                         <a href="{{ url('persuratan/masuk') }}" class="sidebar-link " aria-expanded="false">
                             <i class="fas fa-circle"></i>
                             <span class="hide-menu"> Surat Masuk </span>
                         </a>
                     </li>
                 </ul>
             </li>

         </ul>
     </nav>
     <!-- End Sidebar navigation -->
 </div>
 <!-- End Sidebar scroll-->

@extends('layout/main')
@push('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('title', 'Surat')
@section('navbarcontent')
    <h4 class="page-title">Data Surat</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('persuratan') }}">Surat</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>
    </div>
@endsection
@push('after-script')
    <!-- add before </body> -->
    <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
    <!-- add before </body> -->
    <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
    <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
@endpush

@section('style')
    <style>
        .filepond--root {
            height: 10em;
        }

    </style>
@endsection

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title mb-5">
                    <div class="col-md-4  float-left">
                        <h3>Data Surat</h3>
                    </div>
                    <div class="col-md-6  float-right">
                        <div class="btn-group float-right mb-3">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">Action</button>
                            <div class="dropdown-menu mr-5 mt-1">
                                <button class="dropdown-item" id="btnGenerate"><i
                                        class="mdi mdi-shape-circle-plus mr-2"></i> Generate Surat </button>

                            </div>
                        </div><!-- /btn-group -->
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered tableSurat">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Surat</th>
                                <th>Kategori Surat</th>
                                <th>Keperluan Surat</th>
                                <th>User</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>


        {{-- MODAL --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="delete-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" name="btnDelete" id="btnDelete">Hapus</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}



        {{-- MODAL --}}
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="detail-modal">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detail Surat</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body badan">
                        <form id="form-detail" name="form-detail">
                            @method('put')

                            <input type="hidden" name="id" id="id">

                            <div class="form-group">
                                <label for="" class="col-form-label">No. Surat</label>
                                <input type="text" class="form-control" name="no_surat" id="no_surat" autocomplete="off"
                                    disabled>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Nama File</label>
                                <input type="text" class="form-control" name="nama_file" id="nama_file" autocomplete="off"
                                    disabled>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Tanggal Masuk</label>
                                <input type="text" class="form-control" name="tgl_masuk" id="tgl_masuk" autocomplete="off"
                                    disabled>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Tanggal Keluar</label>
                                <input type="text" class="form-control" name="tgl_keluar" id="tgl_keluar" autocomplete="off"
                                    disabled>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Keterangan</label>
                                <textarea class="form-control" name="keterangan" id="keterangan" cols="20" rows="10"
                                    autocomplete="off" disabled></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                {{-- <a type="button" href="{{ route('surat.download', $data->file) }}" target="_BLANK" class="btn btn-info">Download</a> --}}
                                <button type="button" class="btn btn-primary" id="btnDownload" name="btnDownload"
                                    data-dismiss="modal">Download</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}

        {{-- MODAL --}}
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="modal-edit">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Surat</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="form-edit" name="form-edit">
                            @method('put')

                            <input type="hidden" name="edit_id" id="edit_id">

                            <div class="form-group">
                                <label for="" class="col-form-label">No. Surat</label>
                                <input type="text" class="form-control" name="edit_no_surat" id="edit_no_surat"
                                    autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Nama Surat</label>
                                <input type="text" class="form-control" name="edit_nama" id="edit_nama" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Keterangan</label>
                                <textarea class="form-control" name="edit_keterangan" id="edit_keterangan" cols="20"
                                    rows="10" autocomplete="off"></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" id="btnSaveEdit">Simpan Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}

        {{-- MODAL --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="modal-generate">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content modal-lg">
                    <div class="modal-header">
                        <h5 class="modal-title">Generate Surat</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" id="form-generate" name="form-generate">
                            @csrf
                            <div class="form-group">
                                <label for="" class="col-form-label">Nama Surat</label>
                                <input type="text" class="form-control" name="g_nama" id="g_nama" required>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-form-label">Keperluan</label>
                                <select class="form-control select2" id="keperluan" name="g_keperluan" id="g_keperluan"
                                    style="height: 36px;width: 100%;" aria-placeholder="Pilih Keperluan.." required>
                                    <option value="" disabled selected>-- Pilih Keperluan --</option>
                                    @foreach ($keperluan as $row)
                                        <option value="{{ $row->id }}">{{ $row->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-form-label">Nomor Surat</label>
                                <input type="text" class="form-control" name="g_nosurat" id="g_nosurat" required readonly>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-form-label">Template</label>
                                <select class="form-control select2" name="g_template" id="g_template"
                                    style="height: 36px;width: 100%;" aria-placeholder="Pilih Template.." required>
                                    @foreach ($template as $row)
                                        <option value="{{ $row->id }}">{{ $row->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-form-label">Tanggal</label>
                                <input type="text" class="form-control mydatepicker" name="g_tgl" placeholder="mm/dd/yyyy"
                                    required autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="" class="col-form-label">Keterangan</label>
                                <textarea class="form-control" name="keterangan" id="keterangan" cols="20" rows="10"
                                    autocomplete="off"></textarea>
                            </div>

                            <div class="modal-footer bg-whitesmoke br">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" id="btnSaveGenerate">Generate</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}

        {{-- MODAL --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="upload-modal">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Upload PDF</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="upload-pdf" name="upload-pdf" method="POST">
                            @method('POST')
                            <input type="hidden" name="id_surat" id="id_surat">
                            <div class="form-group">
                                <input type="file" class="form-control" name="docs" id="docs">
                            </div>

                            <div class="modal-footer bg-whitesmoke br">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" id="btnUploadPDF">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}



        {{-- MODAL --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="pdf-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title">Konfirmasi Hapus PDF</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" name="btnHapusPDF" id="btnHapusPDF">Hapus</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}
    @endsection

    @push('after-script')
        <script>
            @if (session('sukses'))
                const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
                })
            
                Toast.fire({
                icon: 'success',
                title: `{{ session('sukses') }}`
                });
            @endif
            $(document).ready(function() {
                //  $('#summernoteTemplate').summernote({
                //     toolbar: [
                //         // [groupName, [list of button]]
                //         // ['style', ['bold', 'italic', 'underline', 'clear']],
                //         // ['fontsize', ['fontsize']],
                //         // ['fontname', ['fontname']],
                //         // ['color', ['color']],
                //         // ['para', ['ul', 'ol', 'paragraph']],
                //         // ['height', ['height']],
                //         // ['insert', ['picture']],
                //         // ['table', ['table']],
                //         // ['view', ['fullscreen', 'codeview', 'help']],
                //     ],
                //     height: 300,
                // });
                // $('#summernoteTemplate').summernote('disable');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var t = $('.tableSurat').DataTable({
                    lengthMenu: [
                        [5, 10, 50, -1],
                        [5, 10, 50, "All"]
                    ],
                    bAutoWidth: false,
                    processing: true,
                    serverSide: true,
                    language: {
                        processing: '<div class="fas fa-circle-notch fa-3x fa-spin style="display: inline-block;"></div>'
                    },
                    ajax: {
                        url: "{{ url('/persuratan') }}",
                        type: "GET",
                    },
                    columns: [{
                            data: 'no',
                            name: 'no'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'namaKategori',
                            name: 'namaKategori'
                        },
                        {
                            data: 'keperluan',
                            name: 'keperluan'
                        },
                        {
                            data: 'namaUser',
                            name: 'namaUser'
                        },
                        {
                            data: 'status',
                            name: 'status'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        },
                    ],

                });

                t.on('draw.dt', function() {
                    var PageInfo = $('.tableSurat').DataTable().page.info();
                    t.column(0, {
                        page: 'current'
                    }).nodes().each(function(cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                    });
                });

                $('#keperluan').change(function() {
                    $.get('{{ url('') }}/persuratan/generate_nosurat/' + this.value, function(data) {
                        $('#g_nosurat').val(data);
                    });
                });

            });

            // Detail ========================================================================
            $('body').on('click', '.detail', function() {
                data_id = $(this).attr('id');

                $.get('{{ url('') }}/persuratan/show/' + data_id, function(data) {
                    $('#detail-modal').modal('show');

                    $('#id').val(data.id);
                    $('#no_surat').val(data.no_surat);
                    $('#tgl_masuk').val(data.tgl_masuk);
                    $('#tgl_keluar').val(data.tgl_keluar);
                    $('#nama_file').val(data.nama_file);
                    $('#keterangan').val(data.keterangan);

                });
            });

            $('#btnDownload').click(function() {
                nama_file = $('#nama_file').val();
                window.open("{{ url('') }}/persuratan/download/" + nama_file);
            });


            // Edit========================================================================
            $('body').on('click', '.edit', function() {
                data_id = $(this).attr('id');
                $('#btnSaveEdit').html('Simpan Edit');
                var x = document.getElementById("btnSaveEdit");
                x.disabled = false;

                $.get('{{ url('') }}/persuratan/show/' + data_id, function(data) {
                    $('#modal-edit').modal('show');
                    console.log(data.nama);
                    $('#edit_id').val(data.id);
                    $('#edit_no_surat').val(data.no_surat);
                    $('#edit_nama').val(data.nama);
                    $('#edit_keterangan').val(data.keterangan);
                });
            });

            $("#form-edit").validate({
                submitHandler: function(form) {
                    $('#btnSaveEdit').html('Sending..');
                    var x = document.getElementById("btnSaveEdit");
                    x.disabled = true;
                    $.ajax({
                        data: $('#form-edit')
                            .serialize(), //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request

                        url: "{{ url('') }}/persuratan/update/" + data_id, //url simpan data
                        type: "POST", //karena simpan kita pakai method POST
                        dataType: 'json', //data tipe kita kirim berupa JSON
                        success: function(data) { //jika berhasil 
                            $('#form-edit').trigger("reset"); //form reset
                            $('#modal-edit').modal('hide'); //modal hide
                            var oTable = $('.tableSurat').dataTable(); //inialisasi datatable
                            oTable.fnDraw(false); //reset datatable
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal
                                        .resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: `Berhasil Update Surat`
                            });
                        },
                        error: function(data) { //jika error tampilkan error pada console
                            console.log('Error:', data);
                        }
                    });
                }
            });

            // Delete
            $(document).on('click', '.delete', function() {
                dataId = $(this).attr('id');
                $('#btnDelete').text('Hapus');
                $('#modal-title').html("Konfirmasi Hapus Surat");
                $('#delete-modal').modal('show');
            });

            $('#btnDelete').click(function() {
                $.ajax({

                    url: "{{ url('') }}/persuratan/destroy/" + dataId, //eksekusi ajax ke url ini
                    type: 'GET',
                    beforeSend: function() {
                        $('#btnDelete').text('Tunggu..'); //set text untuk tombol hapus
                    },
                    success: function(data) { //jika sukses
                        setTimeout(function() {
                            $('#delete-modal').modal('hide'); //sembunyikan konfirmasi modal
                            var oTable1 = $('.tableSurat').dataTable();
                            oTable1.fnDraw(false); //reset datatable
                        });
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            didOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'warning',
                            title: `Berhasil Destroy Surat`
                        });
                    }
                })
            });





            // Generate modal
            $('#btnGenerate').click(function() {
                $('#form-generate')[0].reset();
                // $('.select2').empty();
                $('#btnSaveGenerate').text('Generate');
                var x = document.getElementById("btnSaveGenerate");
                x.disabled = false;
                $('#modal-generate').modal('show');

            });

            $('#btnSaveGenerate').click(function() {
                $.ajax({

                    data: $('#form-generate').serialize(),
                    url: "{{ route('surat.generate') }}", //eksekusi ajax ke url ini
                    type: "POST",
                    beforeSend: function() {
                        $('#btnSaveGenerate').text('Tunggu..'); //set text untuk tombol hapus
                        var x = document.getElementById("btnSaveGenerate");
                        x.disabled = true;
                    },
                    success: function(data) { //jika sukses
                        setTimeout(function() {
                            $('#modal-generate').modal('hide'); //sembunyikan konfirmasi modal
                            var oTable1 = $('.tableSurat').dataTable();
                            oTable1.fnDraw(false); //reset datatable
                        });

                        window.location.href = data.file_name;
                        const Toast = Swal.fire({
                            icon: 'success',
                            title: 'Success !!',
                            text: 'Berhasil Generate Surat'
                        });
                    }
                })
            });

            // Register the plugin
            FilePond.registerPlugin(FilePondPluginFileValidateType);
            const inputElement = document.querySelector('input[id="docs"]');
            FilePond.create(
                inputElement, {
                    credits: false,
                    maxFileSize: "3000000",
                    acceptedFileTypes: [
                        // 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        // 'application/msword',
                        'application/pdf',
                    ],
                    fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
                        resolve(type);
                    }),
                    server: {
                        url: "{{ url('persuratan/upload') }}",
                        type: "POST",
                        headers: {
                            "X-CSRF-TOKEN": '{{ csrf_token() }}'
                        },
                        process: `/process`,
                        revert: `/revert`,
                        fetch: null
                    }
                }
            );

            // Upload pdf
            $('body').on('click', '.uploadPDF', function() {
                $('#btnUploadPDF').html('Upload');
                var x = document.getElementById("btnUploadPDF");
                x.disabled = false;
                data_id = $(this).attr('id');
                $('#upload-modal').modal('show');
                $('#id_surat').val(data_id);

            });

            $("#upload-pdf").validate({
                submitHandler: function(form) {
                    $('#btnUploadPDF').html('Sending..');
                    var x = document.getElementById("btnUploadPDF");
                    x.disabled = true;
                    $.ajax({
                        data: $('#upload-pdf')
                            .serialize(), //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request

                        url: "{{ route('surat.uploadPDF') }}", //url simpan data
                        type: "POST", //karena simpan kita pakai method POST
                        dataType: 'json', //data tipe kita kirim berupa JSON
                        success: function(data) { //jika berhasil 
                            $('#upload-pdf').trigger("reset"); //form reset
                            $('#upload-modal').modal('hide'); //modal hide
                            var oTable = $('.tableSurat').dataTable(); //inialisasi datatable
                            oTable.fnDraw(false); //reset datatable
                            // const Toast = Swal.mixin({
                            //     toast: true,
                            //     position: 'top-end',
                            //     showConfirmButton: false,
                            //     timer: 3000,
                            //     timerProgressBar: true,
                            //     didOpen: (toast) => {
                            //         toast.addEventListener('mouseenter', Swal.stopTimer)
                            //         toast.addEventListener('mouseleave', Swal
                            //             .resumeTimer)
                            //     }
                            // })

                            // Toast.fire({
                            //     icon: 'success',
                            //     title: `Berhasil Update Surat`
                            // });
                            const Toast = Swal.fire({
                                icon: 'success',
                                title: 'Success !!',
                                text: 'Berhasil Upload PDF File'
                            });
                        },
                        error: function(data) { //jika error tampilkan error pada console
                            console.log('Error:', data);
                        }
                    });
                }
            });



            $(document).on('click', '.deletePDF', function() {
                var x = document.getElementById("btnHapusPDF");
                x.disabled = false;
                dataId = $(this).attr('id');
                $('#btnHapusPDF').text('Hapus');
                $('#pdf-modal').modal('show');
            });

            $('#btnHapusPDF').click(function() {
                $.ajax({

                    url: "{{ route('surat.hapusPDF', '') }}" + '/' + dataId, //eksekusi ajax ke url ini
                    type: 'GET',
                    beforeSend: function() {
                        $('#btnHapusPDF').text('Tunggu..'); //set text untuk tombol hapus
                    },
                    success: function(data) { //jika sukses
                        setTimeout(function() {
                            $('#pdf-modal').modal('hide'); //sembunyikan konfirmasi modal
                            var oTable1 = $('.tableSurat').dataTable();
                            oTable1.fnDraw(false); //reset datatable
                        });
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            didOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'warning',
                            title: `Berhasil Hapus PDF`
                        });
                    }
                })
            });
        </script>
    @endpush

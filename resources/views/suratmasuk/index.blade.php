@extends('layout/main')
@push('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('title', 'Surat')
@section('navbarcontent')
    <h4 class="page-title">Data Surat Masuk</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('persuratan') }}">Surat</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>
    </div>
@endsection
@push('after-script')
    <!-- add before </body> -->
    <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
    <!-- add before </body> -->
    <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
    <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
@endpush

@section('style')
    <style>
        .filepond--root {
            height: 10em;
        }

    </style>
@endsection

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title mb-5">
                    <div class="col-md-4  float-left">
                        <h3>Data Surat Masuk</h3>
                    </div>
                    <div class="col-md-6  float-right">
                        <div class="btn-group float-right mb-3">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">Action</button>
                            <div class="dropdown-menu mr-5 mt-1">
                                <button class="dropdown-item" id="btnUpload"><i class="mdi mdi-cloud-upload mr-2"></i>
                                    Upload Surat Masuk</button>

                            </div>
                        </div><!-- /btn-group -->
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered tableSurat">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Surat</th>
                                <th>Tanggal Surat Masuk</th>
                                <th>User</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>


        {{-- MODAL --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="delete-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" name="btnDelete" id="btnDelete">Hapus</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}

        {{-- MODAL --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="upload-modal">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Upload Surat Masuk</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('persuratan/suratMasuk') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="" class="col-form-label">Nama Surat</label>
                                <input type="text" class="form-control" name="nama" id="nama" required>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label mr-4">No. Surat</label><span style="color:red;">Di isi
                                    jika ada ! </span>
                                <input type="text" class="form-control" name="nosurat" id="nosurat">
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Keterangan</label>
                                <textarea name="up_keterangan" cols="30" rows="5" class="form-control"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Tanggal Surat Dibuat</label>
                                <input type="text" class="form-control mydatepicker" name="tgl_dibuat"
                                    placeholder="mm/dd/yyyy" required autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Upload File Docs</label>
                                <input type="file" class="form-control" name="docs" id="docs">
                            </div>

                            <div class="modal-footer bg-whitesmoke br">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}

        {{-- MODAL --}}
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="detail-modal">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Detail Surat</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body badan">
                        <form id="form-detail" name="form-detail">
                            @method('put')

                            <input type="hidden" name="id" id="id">

                            <div class="form-group">
                                <label for="" class="col-form-label">No. Surat</label>
                                <input type="text" class="form-control" name="no_surat" id="no_surat" autocomplete="off"
                                    disabled>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Nama File</label>
                                <input type="text" class="form-control" name="nama_file" id="nama_file" autocomplete="off"
                                    disabled>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Tanggal Masuk</label>
                                <input type="text" class="form-control" name="tgl_masuk" id="tgl_masuk" autocomplete="off"
                                    disabled>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Tanggal Surat Dibuat</label>
                                <input type="text" class="form-control" name="tgl_keluar" id="tgl_keluar" autocomplete="off"
                                    disabled>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Keterangan</label>
                                <textarea class="form-control" name="keterangan" id="keterangan" cols="20" rows="10"
                                    autocomplete="off" disabled></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                {{-- <a type="button" href="{{ route('surat.download', $data->file) }}" target="_BLANK" class="btn btn-info">Download</a> --}}
                                <button type="button" class="btn btn-primary" id="btnDownload" name="btnDownload"
                                    data-dismiss="modal">Download</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}

        {{-- MODAL --}}
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="modal-edit">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Surat</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="form-edit" name="form-edit">
                            @method('put')

                            <input type="hidden" name="edit_id" id="edit_id">

                            <div class="form-group">
                                <label for="" class="col-form-label">No. Surat</label>
                                <input type="text" class="form-control" name="edit_no_surat" id="edit_no_surat"
                                    autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Nama Surat</label>
                                <input type="text" class="form-control" name="edit_nama" id="edit_nama" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Tanggal Surat Dibuat</label>
                                <input type="text" class="form-control mydatepicker" name="edit_tgl_dibuat"
                                    id="edit_tgl_dibuat" placeholder="mm/dd/yyyy" required autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="" class="col-form-label">Keterangan</label>
                                <textarea class="form-control" name="edit_keterangan" id="edit_keterangan" cols="20"
                                    rows="10" autocomplete="off"></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" id="btnSaveEdit">Simpan Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}



    @endsection

    @push('after-script')
        <script>
            @if (session('sukses'))
                const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
                })
            
                Toast.fire({
                icon: 'success',
                title: `{{ session('sukses') }}`
                });
            @endif

            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var t = $('.tableSurat').DataTable({
                    lengthMenu: [
                        [5, 10, 50, -1],
                        [5, 10, 50, "All"]
                    ],
                    bAutoWidth: false,
                    processing: true,
                    serverSide: true,
                    language: {
                        processing: '<div class="fas fa-circle-notch fa-3x fa-spin style="display: inline-block;"></div>'
                    },
                    ajax: {
                        url: "{{ url('/persuratan/masuk') }}",
                        type: "GET",
                    },
                    columns: [{
                            data: 'no',
                            name: 'no'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'tgl_masuk',
                            name: 'tgl_masuk'
                        },
                        {
                            data: 'namaUser',
                            name: 'namaUser'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        },
                    ],

                });

                t.on('draw.dt', function() {
                    var PageInfo = $('.tableSurat').DataTable().page.info();
                    t.column(0, {
                        page: 'current'
                    }).nodes().each(function(cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                    });
                });

                $('#keperluan').change(function() {
                    $.get('{{ url('') }}/persuratan/generate_nosurat/' + this.value, function(data) {
                        $('#g_nosurat').val(data);
                    });
                });

            });

            // Detail ========================================================================
            $('body').on('click', '.detail', function() {
                data_id = $(this).attr('id');

                $.get('{{ url('') }}/persuratan/show/' + data_id, function(data) {
                    $('#detail-modal').modal('show');

                    $('#id').val(data.id);
                    $('#no_surat').val(data.no_surat);
                    $('#tgl_masuk').val(data.tgl_masuk);
                    $('#tgl_keluar').val(data.tgl_keluar);
                    $('#nama_file').val(data.nama_file);
                    $('#keterangan').val(data.keterangan);

                });
            });

            $('#btnDownload').click(function() {
                nama_file = $('#nama_file').val();
                window.location.href = "{{ url('') }}/persuratan/download/" + nama_file;
            });


            // Edit========================================================================
            $('body').on('click', '.edit', function() {
                data_id = $(this).attr('id');

                $('#btnSaveEdit').html('Simpan Edit');

                $.get('{{ url('') }}/persuratan/show/' + data_id, function(data) {
                    $('#modal-edit').modal('show');
                    console.log(data.nama);
                    $('#edit_id').val(data.id);
                    $('#edit_no_surat').val(data.no_surat);
                    $('#edit_nama').val(data.nama);
                    $('#edit_keterangan').val(data.keterangan);
                    $('#edit_tgl_dibuat').val(data.tgl_keluar);
                });
            });

            $("#form-edit").validate({
                submitHandler: function(form) {
                    $('#btnSaveEdit').html('Sending..');
                    $.ajax({
                        data: $('#form-edit')
                            .serialize(), //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request

                        url: "{{ url('') }}/persuratan/update/" + data_id, //url simpan data
                        type: "POST", //karena simpan kita pakai method POST
                        dataType: 'json', //data tipe kita kirim berupa JSON
                        success: function(data) { //jika berhasil 
                            $('#form-edit').trigger("reset"); //form reset
                            $('#modal-edit').modal('hide'); //modal hide
                            var oTable = $('.tableSurat').dataTable(); //inialisasi datatable
                            oTable.fnDraw(false); //reset datatable
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal
                                        .resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: `Berhasil Update Surat`
                            });
                        },
                        error: function(data) { //jika error tampilkan error pada console
                            console.log('Error:', data);
                        }
                    });
                }
            });

            // Delete
            $(document).on('click', '.delete', function() {
                dataId = $(this).attr('id');
                $('#btnDelete').text('Hapus');
                $('#modal-title').html("Konfirmasi Hapus Surat");
                $('#delete-modal').modal('show');
            });

            $('#btnDelete').click(function() {
                $.ajax({

                    url: "{{ url('') }}/persuratan/destroy/" + dataId, //eksekusi ajax ke url ini
                    type: 'GET',
                    beforeSend: function() {
                        $('#btnDelete').text('Tunggu..'); //set text untuk tombol hapus
                    },
                    success: function(data) { //jika sukses
                        setTimeout(function() {
                            $('#delete-modal').modal('hide'); //sembunyikan konfirmasi modal
                            var oTable1 = $('.tableSurat').dataTable();
                            oTable1.fnDraw(false); //reset datatable
                        });
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            didOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'warning',
                            title: `Berhasil Destroy Surat`
                        });
                    }
                })
            });



            // Upload modal
            $('#btnUpload').click(function() {
                $('#upload-modal').modal('show');
            });

            // Register the plugin
            FilePond.registerPlugin(FilePondPluginFileValidateType);
            const inputElement = document.querySelector('input[id="docs"]');
            FilePond.create(
                inputElement, {
                    credits: false,
                    maxFileSize: "3000000",
                    acceptedFileTypes: [
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        'application/msword',
                        'application/pdf',
                    ],
                    fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
                        resolve(type);
                    }),
                    server: {
                        url: "{{ url('persuratan/upload') }}",
                        type: "POST",
                        headers: {
                            "X-CSRF-TOKEN": '{{ csrf_token() }}'
                        },
                        process: '/process',
                        revert: '/revert',
                        fetch: null
                    }
                }
            );
        </script>
    @endpush

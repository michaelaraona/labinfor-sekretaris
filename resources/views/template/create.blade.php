@extends('layout/main')
@section('title' , 'Create Template')
@section('navbarcontent')
    <h4 class="page-title">Create Template</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('template') }}">Template</a></li>
                <li class="breadcrumb-item active" aria-current="page">Create</li>
            </ol>
        </nav>
    </div>
@endsection

@section('content')
    <div class="col-md-12">
        <div class="card">
            
            <div class="card-body">
                <div class="card-title mb-5">
                    <div class="col-md-4  float-left">
                        <h3>Create Template</h3>
                    </div>
                </div>
                <form  action="{{ url('template/store') }}" method="POST">
                    @csrf
                    <div class="form-group mt-4">
                        <label>Nama Surat : </label>
                        <input type="text" class="form-control" name="nama" id="nama" autocomplete="off" required>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="" class="col-form-label">Pilih Kategori</label>
                            <select class="form-control select2" name="kategori" id="kategori" style="height: 36px;width: 100%;" aria-placeholder="Pilih Kategori.." required>
                                @foreach ($kategori as $row)
                                    <option value="{{ $row->id }}">{{ $row->nama }}</option>
                                @endforeach
                            </select>      
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Template Surat : </label>
                        <textarea id="summernote" name="surat" required></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnClose">Close</button>
                        <button type="submit" class="btn btn-success" id="btnSave">Generate</button>
                    </div>
                </form>
            </div>
    </div>
@endsection
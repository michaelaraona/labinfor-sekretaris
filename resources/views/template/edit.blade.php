@extends('layout/main')
@section('title' , 'Edit Template')
@section('navbarcontent')
    <h4 class="page-title">Edit Template</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('template') }}">Template</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
            </ol>
        </nav>
    </div>
@endsection

@section('content')
    <div class="col-md-12">
        <div class="card">
            
            <div class="card-body">
                <div class="card-title mb-5">
                    <div class="col-md-4  float-left">
                        <h3>Edit Template</h3>
                    </div>
                </div>
                <form  action="{{ url('template/update/'.$template->id) }}" method="POST">
                    @method('put')
                    @csrf
                    <div class="form-group mt-4">
                        <label>Nama Surat : </label>
                        <input type="text" class="form-control" name="nama" id="nama" autocomplete="off" value="{{ $template->nama }}" required>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="" class="col-form-label">Pilih Kategori</label>
                            <select class="form-control select2" name="kategori" id="kategori" style="height: 36px;width: 100%;" aria-placeholder="Pilih Kategori.." required>
                                @foreach ($kategori as $row)
                                    <option value="{{ $row->id }}" {{ $row->id == $template->idKategori ? 'selected' : '' }}>{{ $row->nama }}</option>
                                @endforeach
                            </select>      
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-secondary" href="{{ url('template') }}">Back</a>
                        <button type="submit" class="btn btn-success">Change Template</button>
                    </div>
                </form>
            </div>
    </div>
@endsection

@push('after-script')
    <script>
        
    </script>
@endpush
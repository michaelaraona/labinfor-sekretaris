@extends('layout/main')
@push('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('title', 'Template')
@section('navbarcontent')
    <h4 class="page-title">Template</h4>
    <div class="ml-auto text-right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('template') }}">Template</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>
    </div>
@endsection
@push('after-script')
    <!-- add before </body> -->
    <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
    <!-- add before </body> -->
    <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
    <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
@endpush

@section('style')
    <style>
        .filepond--root {
            height: 10em;
        }

    </style>
@endsection

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title mb-5">
                    <div class="col-md-4  float-left">
                        <h3>Data Template</h3>
                    </div>
                    <div class="col-md-6  float-right">
                        <div class="btn-group float-right mb-3">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">Action</button>
                            <div class="dropdown-menu mr-5 mt-1">
                                {{-- <a href="{{ url('/template/create') }}" id="btnGenerate" class="dropdown-item" href="#"><i class="far fa-file-word mr-2"></i>Generate Template</a> --}}
                                <button class="dropdown-item" id="btnUpload"><i class="mdi mdi-cloud-upload mr-2"></i>
                                    Upload Template</button>
                                <button class="dropdown-item" id="btnContoh"><i class="mdi mdi-download mr-2"></i>
                                    Contoh Template</button>
                                <button class="dropdown-item trash" id="btnTrash"><i class="far fa-trash-alt mr-2"></i>
                                    Trash</button>
                            </div>
                        </div><!-- /btn-group -->
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered tableTemplate">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Kategori</th>
                                <th>Nama File</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>


        {{-- MODAL --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="delete-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" name="btnDelete" id="btnDelete">Hapus</button>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}

        {{-- MODAL --}}
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="trash-modal">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Trash Template</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body badan">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered tableTrash">
                                <thead>
                                    <tr>
                                        <th width="5%">No.</th>
                                        <th>Nama</th>
                                        <th>Kategori</th>
                                        <th>Nama File</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                {{-- <tbody>
                                    @foreach ($trash as $row)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $row->nama }}</td>
                                            <td>{{ $row->kategori }}</td>
                                            <td>{{ $row->jenis }}</td>
                                            <td align="center">
                                                <button type="button" class="pulihkan btn btn-success" idPulihkan="{{ $row->id }}" id="btnPulihkan">Pulihkan</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody> --}}
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">

                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}

        {{-- MODAL --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="upload-modal">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Upload Template</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('template/store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="" class="col-form-label">Nama Surat</label>
                                <input type="text" class="form-control" name="nama" id="nama">
                            </div>
                            <div class="form-group">
                                <label for="" class="col-form-label">Kategori</label>
                                <select class="form-control select2" name="kategori" id="kategori"
                                    style="height: 36px;width: 100%;" aria-placeholder="Pilih Kategori..">
                                    @foreach ($kategori as $row)
                                        <option value="{{ $row->id }}">{{ $row->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-form-label">Upload File Docs</label>
                                <input type="file" class="form-control" name="docs" id="docs">
                            </div>

                            <div class="modal-footer bg-whitesmoke br">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" id="btnSubmit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Modal --}}

    @endsection

    @push('after-script')
        <script>
            @if (session('sukses'))
                const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
                })
            
                Toast.fire({
                icon: 'success',
                title: `{{ session('sukses') }}`
                });
            @endif

            $(document).ready(function() {
                //  $('#summernoteTemplate').summernote({
                //     toolbar: [
                //         // [groupName, [list of button]]
                //         // ['style', ['bold', 'italic', 'underline', 'clear']],
                //         // ['fontsize', ['fontsize']],
                //         // ['fontname', ['fontname']],
                //         // ['color', ['color']],
                //         // ['para', ['ul', 'ol', 'paragraph']],
                //         // ['height', ['height']],
                //         // ['insert', ['picture']],
                //         // ['table', ['table']],
                //         // ['view', ['fullscreen', 'codeview', 'help']],
                //     ],
                //     height: 300,
                // });
                // $('#summernoteTemplate').summernote('disable');

                var t = $('.tableTemplate').DataTable({
                    lengthMenu: [
                        [5, 10, 50, -1],
                        [5, 10, 50, "All"]
                    ],
                    bAutoWidth: false,
                    processing: true,
                    serverSide: true,
                    language: {
                        processing: '<div class="fas fa-circle-notch fa-3x fa-spin style="display: inline-block;"></div>'
                    },
                    ajax: {
                        url: "{{ url('/template') }}",
                        type: "GET",
                    },
                    columns: [{
                            data: 'no',
                            name: 'no'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'kategori',
                            name: 'kategori'
                        },
                        {
                            data: 'file',
                            name: 'file'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        },
                    ],

                });

                t.on('draw.dt', function() {
                    var PageInfo = $('.tableTemplate').DataTable().page.info();
                    t.column(0, {
                        page: 'current'
                    }).nodes().each(function(cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                    });
                });

                // Trash Table
                var trash = $('.tableTrash').DataTable({
                    lengthMenu: [
                        [5, 10, 50, -1],
                        [5, 10, 50, "All"]
                    ],
                    bAutoWidth: false,
                    processing: true,
                    serverSide: true,
                    language: {
                        processing: '<div class="fas fa-circle-notch fa-3x fa-spin style="display: inline-block;"></div>'
                    },
                    ajax: {
                        url: "{{ url('/template/trash') }}",
                        type: "GET",
                    },
                    columns: [{
                            data: 'no',
                            name: 'no'
                        },
                        {
                            data: 'nama',
                            name: 'nama'
                        },
                        {
                            data: 'kategori',
                            name: 'kategori'
                        },
                        {
                            data: 'file',
                            name: 'file'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        },
                    ],

                });

                trash.on('draw.dt', function() {
                    var PageInfo = $('.tableTrash').DataTable().page.info();
                    trash.column(0, {
                        page: 'current'
                    }).nodes().each(function(cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                    });
                });
            });

            // Edit========================================================================
            $('body').on('click', '.edit', function() {
                data_id = $(this).attr('id');
                window.location = '{{ url('') }}/template/edit/' + data_id;
            });

            // Delete
            $(document).on('click', '.delete', function() {
                var x = document.getElementById("btnDelete");
                x.disabled = false;
                dataId = $(this).attr('id');
                $('#btnDelete').text('Hapus');
                $('#modal-title').html("Konfirmasi Hapus Template");
                $('#delete-modal').modal('show');
            });

            $('#btnDelete').click(function() {
                $.ajax({

                    url: "{{ url('') }}/template/delete/" + dataId, //eksekusi ajax ke url ini
                    type: 'GET',
                    beforeSend: function() {
                        $('#btnDelete').text('Tunggu..'); //set text untuk tombol hapus
                        var x = document.getElementById("btnDelete");
                        x.disabled = true;
                    },
                    success: function(data) { //jika sukses
                        setTimeout(function() {
                            $('#delete-modal').modal('hide'); //sembunyikan konfirmasi modal
                            var oTable1 = $('.tableTemplate').dataTable();
                            var oTable2 = $('.tableTrash').dataTable();
                            oTable1.fnDraw(false); //reset datatable
                            oTable2.fnDraw(false); //reset datatable
                        });
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            didOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'warning',
                            title: `Berhasil Delete Template`
                        });
                    }
                })
            });

            //Pulihkan
            $('#btnTrash').click(function() {
                $('#trash-modal').modal('show');
                $('#btnPulihkan').text('Pulihkan');
            });;

            $(document).on('click', '#btnPulihkan', function() {
                dataId = $(this).attr('idPulihkan');
                $.ajax({

                    url: "{{ url('') }}/template/pulihkan/" + dataId, //eksekusi ajax ke url ini
                    type: 'GET',
                    success: function(data) { //jika sukses
                        setTimeout(function() {
                            $('#trash-modal').modal('hide'); //sembunyikan konfirmasi modal
                            var oTable1 = $('.tableTemplate').dataTable();
                            var oTable2 = $('.tableTrash').dataTable();
                            oTable1.fnDraw(false); //reset datatable
                            oTable2.fnDraw(false); //reset datatable
                        });
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            didOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: `Berhasil Pulihkan Template`
                        });
                    }
                })
            });
            $(document).on('click', '.desc', function() {
                data_id = $(this).attr('id');
                $.get('{{ url('') }}/template/show/' + data_id, function(data) {
                    $('#show-modal').modal('show');
                    $('#suratShow').html(data.surat);
                    $('#suratExport').val(data.surat);
                })
            });

            // Upload modal
            $('#btnUpload').click(function() {
                $('#upload-modal').modal('show');
            });

            function disableSubmit() {
                var x = document.getElementById("btnSubmit");
                x.disabled = true;
            }

            // Register the plugin
            FilePond.registerPlugin(FilePondPluginFileValidateType);
            const inputElement = document.querySelector('input[id="docs"]');
            FilePond.create(
                inputElement, {
                    credits: false,
                    maxFileSize: "3000000",
                    acceptedFileTypes: [
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        'application/msword',
                    ],
                    fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
                        resolve(type);
                    }),
                    server: {
                        url: "{{ url('template/upload') }}",
                        type: "POST",
                        headers: {
                            "X-CSRF-TOKEN": '{{ csrf_token() }}'
                        },
                        process: '/process',
                        revert: '/revert',
                        fetch: null
                    }
                }
            );

            // Contoh
            $('#btnContoh').click(function() {
                window.location.href = "{{ url('') }}/surat/contoh/contoh_template.docx";
            });
        </script>
    @endpush

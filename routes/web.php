<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::group(['middleware' => 'SekretarisMiddleware'], function () {
        Route::prefix('/keperluan')->group(function () {
            Route::get('/', 'KeperluanController@index')->name('index');
            Route::post('/store', 'KeperluanController@store')->name('store');
            Route::get('/json', 'KeperluanController@json')->name('json');
            Route::get('/delete/{id}', 'KeperluanController@delete')->name('delete');
            Route::get('/edit/{id}', 'KeperluanController@edit')->name('edit');
            Route::put('/update/{id}', 'KeperluanController@update')->name('update');
        });

        Route::prefix('/kategori')->group(function () {
            Route::get('/', 'KategoriController@index')->name('index');
            Route::post('/store', 'KategoriController@store')->name('store');
            Route::get('/destroy/{id}', 'KategoriController@destroy')->name('destroy');
            Route::get('/edit/{id}', 'KategoriController@edit')->name('kategori.edit');
            Route::put('/update/{id}', 'KategoriController@update')->name('update');
        });

        Route::prefix('/template')->group(function () {
            Route::get('/', 'TemplateController@index')->name('index');
            Route::get('/create', 'TemplateController@create')->name('create');
            Route::post('/store', 'TemplateController@store')->name('store');
            Route::get('/edit/{id}', 'TemplateController@edit')->name('edit');
            Route::put('/update/{id}', 'TemplateController@update')->name('update');
            Route::get('/delete/{id}', 'TemplateController@delete')->name('delete');
            Route::get('/pulihkan/{id}', 'TemplateController@pulihkan')->name('pulihkan');
            Route::get('/show/{id}', 'TemplateController@show')->name('show');
            Route::get('/trash', 'TemplateController@trash')->name('trash');
            Route::post('/upload/process', 'TemplateController@process')->name('process');
            Route::delete('/upload/revert', 'TemplateController@revert')->name('revert');
            Route::get('/preview/{file}', 'TemplateController@preview')->name('preview');
            Route::get('/contoh', 'TemplateController@contoh')->name('template.contoh');
        });
    });
    Route::prefix('/persuratan')->group(function () {
        Route::get('/', 'SuratController@index')->name('index');
        Route::get('/masuk', 'SuratController@persuratanmasuk')->name('persuratanmasuk');
        Route::get('/show/{id}', 'SuratController@show')->name('show');
        Route::get('/generate_nosurat/{keperluan_id}', 'SuratController@generate_nosurat')->name('generate_nosurat');
        Route::post('/upload/process', 'SuratController@process')->name('process');
        Route::delete('/upload/revert', 'SuratController@revert')->name('revert');
        Route::post('/suratMasuk', 'SuratController@suratMasuk')->name('suratMasuk');
        Route::get('/download/{file}', 'SuratController@download')->name('surat.download');
        Route::get('/destroy/{id}', 'SuratController@destroy')->name('surat.destroy');
        Route::get('/hapusPDF/{id}', 'SuratController@hapusPDF')->name('surat.hapusPDF');
        Route::put('/update/{id}', 'SuratController@update')->name('surat.update');
        Route::post('/generate', 'SuratController@generate')->name('surat.generate');
        Route::post('/uploadPDF', 'SuratController@uploadPDF')->name('surat.uploadPDF');
    });
});

Route::get('clear-all', function () {
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('cache:clear');

    return '<h1>Cache Clear</h1>';
});

Route::get('/', 'AuthController@login')->name('login');
Route::post('/autentikasi', 'AuthController@auth')->name('auth');
Route::get('/logout', 'AuthController@logout')->name('logout');
